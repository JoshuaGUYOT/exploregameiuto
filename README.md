# ExploreGameIutO

## GitPod
Cliquez ci-dessous pour tester notre application:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/JoshuaGUYOT/exploregameiuto)

Il y a 10 comptes de chaques rôles de créé de la forme suivante:

Joueur 1 : MdpJoueur1
Admin 1 : MdpAdmin1
Concepteur 1 : MdpConcepteur1

où les 1 vont jusqu'à 10

## Introduction
Création d'un Escape Game pour les journées portes ouvertes de l'IUT d'Orléans.

Une fois le projet cloné, il suffit de se positionner dans le répertoire ./exploregameiuto/SymfonyExploreGame/ et de lancer la commande : **symfony server:start --no-tls**, mais avant cela il faudra correctement mettre en place l'environnement symfony. Voici la marche à suivre pour installer l'environnement sous Ubuntu.

[TOC]

## I - Installation de l'environnement Symfony et de PHP sur votre poste:

### Installation de symfony:
- wget https://get.symfony.com/cli/installer -O - | bash
- sudo mv /home/_nomDeSession_/.symfony/bin/symfony /usr/local/bin/symfony

### Installation de PHP et de ses composants:
- sudo apt install php
- sudo apt install php-xml
- sudo apt install software-properties-common
- sudo add-apt-repository ppa:ondrej/php
- sudo apt update
- sudo apt upgrade

### Installation de composer:
- sudo apt install composer
- php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
- php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
- php composer-setup.php
- php -r "unlink('composer-setup.php');"

### Installation des librairies pour le projet:
- php composer.phar install
- symfony check:requirements 

![si un encadré vert avec écrit :"[OK] your system is ready to run Symfony projects" tout est bon jusque là !!!](./ReadyToRun.png)

Bravo, c'est presque fini !

### Installation de sqlite3:
- sudo apt-get install php-sqlite3
- Décommenter la ligne 940 de votre fichier php.ini dans /etc/php/8.1/apache2/php.ini

## II - Création et instantiation de la base de données: 

### Création de la base de données:
- php bin/console doctrine:database:create
- php bin/console make:migration
- php bin/console doctrine:migrations:migrate

### Instantiation de la base de données avec un jeu d'essai:
- php bin/console doctrine:fixtures:load

## III - Plus qu'à lancer !!!
- Se placer dans le répertoire ./exploregameiuto/SymfonyExploreGame/ et effectuer la commande **symfony server:start --no-tls**

## IV - Générer la documentation
- php phpDocumentor.phar -d ./src/ -t doc
- Lancer le fichier /exploregameiuto/SymfonyExploreGame/doc/index.html

## V - Problèmes connus:

### Symfony : commande introuvable -> vous ne possèdez pas symfony:
- wget https://get.symfony.com/cli/installer -O - | bash
- sudo mv /home/_nomDeSession_/.symfony/bin/symfony /usr/local/bin/symfony
Puis réésayer la commande **symfony server:start --no-tls**

### No PHP binaries detected -> il n'y a pas php d'installer sur votre machine:
- sudo apt install php
- sudo apt php-xml
- sudo apt install software-properties-common
- sudo add-apt-repository ppa:ondrej/php
- sudo apt update
- sudo apt upgrade
