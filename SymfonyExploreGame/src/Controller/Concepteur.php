<?php
// src/Controller/Concepteur.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Controller\AventureController;
use App\Entity\Film;
use App\Entity\Aventure;
use App\Entity\Reponse;
use App\Form\QuestionType;
use App\Entity\Question;
use App\Form\EtapeType;
use App\Form\FilmType;
use App\Entity\Etape;
use ContainerGBPyNTw\getManagerRegistryAwareConnectionProviderService;
use Doctrine\ORM\Cache\DefaultCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Test\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;

use function PHPUnit\Framework\equalTo;

class Concepteur extends AbstractController
{
    /**
     * Cette fonction renvoie la liste des aventures
     * @param ManagerRegistry $doctrine C'est l'ORM qui est l'intermédiaire nos applications et la base de données
     * @return Response Return le rendu au acceuil.php.twig
    */
    public function accueil(ManagerRegistry $doctrine): Response
    {  
        $result = $doctrine->getRepository(Aventure::class)->findAll();

        if (!$result) {
            $aventure = new Aventure();   
            $aventure->setNomAventure("Aventure");
            $aventure->setEtataventure("En developpement");
            $aventure->setTexteaventure("Texte");
            $doctrine->getManager()->persist($aventure);
            $doctrine->getManager()->flush();

            $result = $doctrine->getRepository(Aventure::class)->findAll();
        }
        $titre = "Accueil";
        $nomAventures = []; 
        $texteAventure = [];
        foreach ($result as $elem) {
            $nomAventures[] = $elem->getNomaventure();
            $texteAventure[] = $elem->getTexteaventure();
        }
        return $this->render('Concepteur/accueil.php.twig',['titre' => $titre, 'nomAventures' => $nomAventures, 'textAventure' => $texteAventure,
                                                             ]);
    }

    /**
     * Cette fonction renvoie les différents champs nécessaire à la modification d'une aventure
     * @param ManagerRegistry $doctrine C'est l'ORM qui est l'intermédiaire nos applications et la base de données
     * @return Response Return le rendu au editAventure.php.twig
    */
    public function editAventure(ManagerRegistry $doctrine): Response
    {
        
        $titre = "Modifier Aventure";
        $nomAventure = $_GET['aventure'];
        $aventure = $doctrine->getRepository(Aventure::class)->find($nomAventure);
       
        $etapes = $doctrine->getRepository(Etape::class)->findBy(['nomaventure' => $nomAventure]);
        $nomEtapes=[];
        $numEtapes=[];
        foreach ($etapes as $elem) {
            $nomEtapes[] = $elem->getNometape();
            $numEtapes[] = $elem->getPlacementaventure();
        }

        $etapesAll = $doctrine->getRepository(Etape::class)->findBy(['nomaventure' => null]);
        $nomEtapesAll = [];
        foreach ($etapesAll as $elem) {
            $nomEtapesAll[] = $elem->getNomEtape();
        }

        $send = $_GET['send'];
        if ($send == 1) {
            $aventure->setTexteaventure($_POST['texteAventure']);
            $aventure->setEtataventure($_POST['etatAventure']);
        }

        $doctrine->getManager()->flush();
        
        return $this->render('Concepteur/editAventure.php.twig',['titre' => $titre,'nomAventure' => $nomAventure,'etatAventure' => $aventure->getEtataventure(),
                                                                'texteAventure'=> $aventure->getTexteaventure(),'etapes' =>  $nomEtapes,'etapesAll' => $nomEtapesAll, 'numEtapes' => $numEtapes]);
    }

    /**
     * Cette fonction permet d'ajouter une étape à une aventure
     * @param ManagerRegistry $doctrine C'est l'ORM qui est l'intermédiaire nos applications et la base de données
     * @return Response
    */
    public function addEtape(ManagerRegistry $doctrine): Response
    {
        $nomAventure = $_GET['aventure'];
        if (!isset($_POST['etapeAll'])) {
            return $this->redirectToRoute('page_acceuil_concepteur');
        }
        $listeEtapeIntoAventure = $doctrine->getRepository(Etape::class)->findBy(['nomaventure' => $nomAventure]);
        $etapeToAdd = $doctrine->getRepository(Etape::class)->findBy(['nometape' => $_POST['etapeAll']])[0];
        if (in_array($etapeToAdd,$listeEtapeIntoAventure)) {
            return $this->redirectToRoute('page_acceuil_concepteur');
        }
        $etapeToAdd->setNomaventure($nomAventure);
        $etapeToAdd->setPlacementaventure(count($listeEtapeIntoAventure)+1);
        $doctrine->getManager()->flush();
        return $this->redirectToRoute('page_modification_aventure',['aventure'=>$nomAventure,'send'=>0]);
    }

    /**
     * Cette fonction permet de supprimer une étape déjà associé à une aventure depuis la page modifier aventure
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function delEtape(ManagerRegistry $doctrine): Response
    {
        if (!isset($_GET['etape'])) {
            return $this->redirectToRoute('page_acceuil_concepteur');
        }
        $etapeToDel = $doctrine->getRepository(Etape::class)->findBy(['nometape' => $_GET['etape']])[0];
        $nomAventure = $etapeToDel->getNomaventure();
        $listeEtapeIntoAventure = $doctrine->getRepository(Etape::class)->findBy(['nomaventure' => $nomAventure]);
        $numE = $etapeToDel->getPlacementaventure();
        foreach($listeEtapeIntoAventure as $elem) {
            if ($elem->getPlacementaventure() > $numE) {
                $elem->setPlacementaventure($elem->getPlacementaventure()-1);
            }
        }
        $etapeToDel->setNomaventure(null);
        $etapeToDel->setPlacementaventure(null);
        $doctrine->getManager()->flush();

        return $this->redirectToRoute('page_modification_aventure',['aventure'=>$_GET['aventure'],'send'=>0]);
     
    } 

    /**
     * Cette fonction permet de supprimer une aventure depuis la page modifier aventure
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function delAventure(ManagerRegistry $doctrine): Response
    {
        if (!isset($_GET['aventure'])) {
            return $this->redirectToRoute('page_acceuil_concepteur');
        }
        $adventureToDel = $doctrine->getRepository(Aventure::class)->find($_GET['aventure']);
        $listeEtapeIntoAventure = $doctrine->getRepository(Etape::class)->findBy(['nomaventure' => $_GET['aventure']]);
        foreach ($listeEtapeIntoAventure as $elem) {
            $elem->setPlacementaventure(null);
            $elem->setNomaventure(null);
        }
        $doctrine->getManager()->remove($adventureToDel);
        $doctrine->getManager()->flush();
        return $this->redirectToRoute('page_acceuil_concepteur');
    }

    /**
     * Cette fonction permet de créer des questions en utilisant le formType de symfony
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function creationQuestion(Request $request, ManagerRegistry $doctrine, EntityManagerInterface $manager): Response
    {
        if (!isset($_GET['idquestion'])) {
            $idquestion = null;
        }
        else {
            $idquestion = $_GET['idquestion'];
        }
        if (!isset($_GET['textequestion'])) {
            $textequestion = null;
        }
        else {
            $textequestion = $_GET['textequestion'];
        }
        if (!isset($_GET['reponsequestion'])) {
            $reponsequestion = null;
        }
        else {
            $reponsequestion = $_GET['reponsequestion'];
        }
        if (!isset($_GET['pointsquestion'])) {
            $pointsquestion = null;
        }
        else {
            $pointsquestion = $_GET['pointsquestion'];
        }
        if (!isset($_GET['error2'])) {
            $error2 = null;
        }
        else {
            $error2 = $_GET['error2'];
        }
        if (!isset($_GET['error3'])) {
            $error3 = null;
        }
        else {
            $error3 = $_GET['error3'];
        }
        if (!isset($_GET['etat'])) {
            $etat = null;
        }
        else {
            $etat = $_GET['etat'];
        }
        if (!isset($_GET['etatCrea'])) {
            $etatCrea = null;
        }
        else {
            $etatCrea = $_GET['etatCrea'];
        }

        $result = $doctrine->getRepository(Question::class)->findAll();

        $nomQuestion = [];
        foreach ($result as $elem) {
            $nomQuestion[] = $elem->getTextequestion();
        }

        $titre = "Question";
        $question = new Question();
        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);
        $error = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $doctrine->getRepository(Question::class)->findAll();
            foreach ($result as $elem) {
                if ($elem->getTextequestion() == ($question->getTextequestion())) {
                    $error = "Cette question existe déjà !";
                    break;
                }
            }
            if ($question->getTextequestion() == "") {
                $error = "Veuillez rentrer un nom valide ";
            }


            if (!isset($error)) {
                $manager->persist($question);
                $manager->flush();
                $id = $question->getIdquestion();
                $reponse = new Reponse();
                $reponse->setLareponse($_POST['lareponse']);
                $reponse->setIdquestion($id);
                $reponse->setLareponse($_POST['lareponse']);
                $reponse->setIdquestion($id);
                $manager->persist($reponse);
                $manager->flush();
                $etatCrea = "Insertion effectué avec succès";
             }
        }

        return $this->render('Concepteur/creationQuestion.php.twig', ['titre' => $titre,'nomQuestion' => $nomQuestion,'form' => $form->createView(),
        'error' => $error, 'error2' => $error2, 'error3' => $error3, 'etatCrea' => $etatCrea, 'etat' => $etat, 'idquestion' => $idquestion, 'textequestion' => $textequestion, 
        'lareponse' => $reponsequestion, 'pointsquestion' => $pointsquestion]);
    }

    /**
     * Cette fonction permet de modifier des questions en vérifiant si une variable a été modifié
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function modifQuestion(ManagerRegistry $doctrine): Response
    {   
        if (isset($_POST['btn-modif'])) {
            $question = $doctrine->getRepository(Question::class)->findby(['textequestion' => $_GET['textequestion']]);
            $question = $question[0];
            $reponse = $doctrine->getRepository(Reponse::class)->findBy(['idquestion' => $question->getIdquestion()]);
            $reponse = $reponse[0];

            if ($_POST['textequestion'] == "") {
                return $this->redirectToRoute("page_creation_question",['textequestion' => null, 'reponsequestion' => null,
                'pointsquestion' => null]);
            }

            if ($_POST['textequestion'] != $question->getTextequestion()) {
                $question->setTextequestion($_POST['textequestion']);
                $doctrine->getManager()->flush();
            }
            
            if ($_POST['reponsequestion'] != $reponse->getLareponse()) {
                $reponse->setLareponse($_POST['reponsequestion']);
                $doctrine->getManager()->flush();
            }

            if ($_POST['pointsquestion'] != $question->getPointsquestion()) {
                $question->setPointsquestion($_POST['pointsquestion']);
                $doctrine->getManager()->flush();
            }

            return $this->redirectToRoute("page_creation_question",['textequestion' => null, 'reponsequestion' => null,
            'pointsquestion' => null]);
        }

        if (isset($_POST['modifQ'])) {
            if ($_POST['modifQ'] != "") {

                $idquestion = null;
                $textequestion = null;
                $reponsequestion = null;
                $pointsquestion = null;

                $question = $doctrine->getRepository(Question::class)->findBy(['textequestion' => $_POST['modifQ']]);
                $question = $question[0];
                $reponse = $doctrine->getRepository(Reponse::class)->findBy(['idquestion' => $question->getIdquestion()]);
                $reponse = $reponse[0];
                $idquestion = $question->getIdquestion();
                $textequestion = $question->getTextequestion();
                $reponsequestion = $reponse->getLareponse();
                $pointsquestion = $question->getPointsquestion();

                return $this->redirectToRoute("page_creation_question",['idquestion' => $idquestion, 'textequestion' => $textequestion, 
                'reponsequestion' => $reponsequestion, 'pointsquestion' => $pointsquestion] );
            }

            else {
                $error2 = "Veuillez sélectionner une question";
                return $this->redirectToRoute("page_creation_question",['error2' => $error2]);
            }
        }

        if (isset($_POST['supprQ'])) {
            $etat = null;
            if ($_POST['supprQ']) {
                $question = $doctrine->getRepository(Question::class)->findBy(['textequestion' => $_POST['supprQ']]);
                $question = $question [0];
                $reponse = $doctrine->getRepository(Reponse::class)->findBy(['idquestion' => $question->getIdquestion()]);
                $reponse = $reponse[0];

                $doctrine->getManager()->remove($question);
                $doctrine->getManager()->remove($reponse);

                $doctrine->getManager()->flush();

                $etat = "Suppression réalisé avec succès";
                return $this->redirectToRoute("page_creation_question", ['etat' => $etat]);
            }

            else {
                $error3 = "Veuillez sélectionner une question" ;
                return $this->redirectToRoute("page_creation_question", ['error3' => $error3]);
            }
            }
    }

    /**
     * Cette fonction permet de créer une étape 
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function creationEtape(Request $request, ManagerRegistry $doctrine, EntityManagerInterface $manager): Response 
    {
        $nometape = null;
        $cox = null;
        $coy = null;
        $placementetape = null;
        $id = null;
        $selected = null;
        $question = null;
        $film = null;
        $etatetape = null;
        $checked = 0;
        $selectedQ = null;
        $selectedF = null;

        if (isset($_GET['nometape'])) {
            $nometape = $_GET['nometape'];
        }
        if (isset($_GET['cox'])) {
            $cox = $_GET['cox'];
        }
        if (isset($_GET['coy'])) {
            $coy = $_GET['coy'];
        }
        if (isset($_GET['placementetape'])) {
            $placementetape = $_GET['placementetape'];
        }
        if (isset($_GET['selected'])) {
            if ($_GET['selected'] != ""){
                $selected = $_GET['selected'];
            }
        }
        if(isset($_GET['selectedQ'])) {
            if ($_GET['selectedQ'] != "") {
                $selectedQ = $_GET['selectedQ'];
            }
        }
        if(isset($_GET['selectedF'])) {
            if ($_GET['selectedF'] != "") {
                $selectedF = $_GET['selectedF'];
            }
        }
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        if (isset($_GET['etatetape'])) {
            $etatetape = $_GET['etatetape'];
        }
        if (isset($_POST['question'])) {
            $question = $_POST['question'];
        }
        if (isset($_POST['film'])) {
            $film = $_POST['film'];
        }
        if (isset($_GET['checked'])) {
            $checked = $_GET['checked'];
        }

        $error = null;
        $etape = new Etape();
        $form = $this->createForm(EtapeType::class, $etape);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            if ( $film == null || $question == null) {
                $error = "Veuillez remplir tous les champs";
            }
            if (!isset($error)) {
                $etape->setIdfilm($film);
                $etape->setIdquestion($question);
                $result = $doctrine->getRepository(Etape::class)->findBy(['nometape' => $etape->getNometape()]);
                if (count($result) > 0) {
                    $error = "Cette étape existe déjà !";
                }
            }
            if (!isset($error)) {

                $result = $doctrine->getRepository(Etape::class)->findAll();

                $count = 1;
                foreach ($result as $elem) {
                    if ($etape->getNomaventure() == $elem->getNomaventure()) {
                        $count += 1;
                    }
                }
                $etape->setPlacementaventure($count);

                $manager->persist($etape);
                $manager->flush();
                return $this->redirectToRoute("page_generate_qrcode",['id' => $etape->getIdetape()]);
            }
        }
        

        $titre = "Etape";

        $result = null;
        $result = $doctrine->getRepository(Aventure::class)->findAll();

        $listeNomAventure = [];
        foreach ($result as $elem) {
            $listeNomAventure[] = $elem->getNomaventure();
        }

        $result = $doctrine->getRepository(Etape::class)->findAll();
        $listeEtape = [];
        $listeAventureParEtape = [];
        $listeId = [];
        foreach ($result as $elem) {
            $listeEtape[] = $elem->getNometape();
            $listeId[] = $elem->getIdetape();
            $listeAventureParEtape[] = $elem->getNomAventure();
        }

        $listeQuestion = [];
        $listeQuestionId = [];
        $result = $doctrine->getRepository(Question::class)->findAll();
        foreach ($result as $elem) {
            $listeQuestion[] = $elem->getTextequestion();
            $listeQuestionId[] = $elem->getIdquestion();
        }

        $listeFilm = [];
        $listeFilmId = [];
        $result = $doctrine->getRepository(Film::class)->findAll();
        foreach ($result as $elem) {
            $listeFilm[] = $elem->getNomfilm();
            $listeFilmId[] = $elem->getIdfilm();
        }

        if (isset($_GET['errorModif'])) {
            $errorModif = $_GET['errorModif'];
        } else {
            $errorModif = null;
        }

        return $this->render('Concepteur/creationEtape.php.twig',['titre' => $titre, 'form' => $form->createView(), 
        'error' => $error, 'nometape' => $nometape , 'cox' => $cox, 'coy' => $coy, 
        'placementetape' => $placementetape, 'selected' => $selected, 'listeNomAventure' => $listeNomAventure, 
        'listeEtape'=>$listeEtape, 'listeAventureParEtape'=>$listeAventureParEtape, 'listeId'=>$listeId, 'id'=>$id,
        'listeQuestionId' => $listeQuestionId, 'listeQuestion' => $listeQuestion,'listeFilm' => $listeFilm, 'listeFilmId' => $listeFilmId,
        'etatetape'=> $etatetape, 'checked' => $checked, 'selectedQ' => $selectedQ,'selectedF' => $selectedF, 'errorModif' => $errorModif]);

    }
    /**
     * Cette fonction permet de modifier l'étape ainsi que de pré-remplir les champs sur la page de modification etape
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function modifEtape(ManagerRegistry $doctrine) : Response
    {
        if (isset($_POST['btn-modif'])) {

            $etape = $doctrine->getRepository(Etape::class)->findby(['idetape' => $_POST['id']]);
            $nometape = null;
            $cox = null;
            $coy = null;
            $selected = null;
            $placementetape = null;
            $id = null;
            $etatetape = false;
            $question = null;
            $film = null;
            $checked = 0;
            $selectedQ = null;
            $selectedF = null;

            if (sizeof($etape) == 0) {
                $errorModif = "Veuillez sélectionner une étape à modifier.";
                return $this->redirectToRoute("page_creation_etape",['errorModif' => $errorModif]);
            }

            $etape = $etape[0];
            $placementetape = $etape->getPlacementaventure();

            if ($_POST['nometape'] != $etape->getNometape()) {
                $etape->setNometape($_POST['nometape']);
                $doctrine->getManager()->flush();
            }
            
            if ($_POST['cox'] != $etape->getPosxqrcode()) {
                $etape->setPosxqrcode($_POST['cox']);
                $doctrine->getManager()->flush();
            }

            if ($_POST['coy'] != $etape->getPosyqrcode()) {
                $etape->setPosyqrcode($_POST['coy']);
                $doctrine->getManager()->flush();
            }

            if (isset($_POST['etatetape'])) {
                if ($_POST['etatetape'] != $etape->getEtatetape()) {
                    $etape->setEtatetape($_POST['etatetape']);
                    $doctrine->getManager()->flush();
                }
            }

            if (isset($_POST['checked']) && $_POST['checked'] != $etape->getEtatetape()) {
                $etape->setEtatetape($_POST['checked']);
                $doctrine->getManager()->flush();
            }
            else {
                $etape->setEtatetape($etatetape);
                $doctrine->getManager()->flush();
            }
            
            if ($_POST['question'] != $etape->getIdquestion() && $_POST['question'] != "") {
                $etape->setIdquestion($_POST['question']);
                $doctrine->getManager()->flush();
            }

            if ($_POST['film'] != $etape->getIdfilm()  && $_POST['film'] != "") {
                $etape->setIdfilm($_POST['film']);
                $doctrine->getManager()->flush();
            }

            if ($_POST['modifE'] != $etape->getNomaventure() && $_POST['modifE'] != "") {
                $etape->setNomaventure($_POST['modifE']);
                $doctrine->getManager()->flush();
            }

            else {
                $error = "Vous n'avez rien modifié !";
            }

            return $this->redirectToRoute("page_creation_etape",['nometape' => $nometape, 'cox' => $cox, 'coy' => $coy, 
            'placementetape' => $placementetape, 'selected' => $selected, 'etatetape' => $etatetape, 'checked' => $checked,
            'selectedQ' => $selectedQ, 'selectedF' => $selectedF]);
        }

        if ($_POST['selectE'] != "") {
            $nometape = null;
            $cox = null;
            $coy = null;
            $selected = null;
            $etatetape = null;
            $checked = null;
            $selectedQ = null;
            $selectedF = null;

            $etape = $doctrine->getRepository(Etape::class)->findby(['idetape' => $_POST['selectE']]);
            $etape = $etape[0];
            $question = $doctrine->getRepository(Question::class)->findby(['idquestion' => $etape->getIdquestion()]);
            $question = $question[0];
            $film = $doctrine->getRepository(Film::class)->findby(['idfilm' => $etape->getIdfilm()]);
            $film = $film[0];
            $nometape = $etape->getNometape();
            $cox = $etape->getPosxqrcode();
            $coy = $etape->getPosyqrcode();
            $selected = $etape->getNomaventure();
            $id = $etape->getIdetape();
            $checked = $etape->getEtatetape();
            $selectedQ = $question->getTextequestion();
            $selectedF = $film->getNomfilm();

            return $this->redirectToRoute("page_creation_etape",['nometape' => $nometape, 'cox' => $cox, 'coy' => $coy, 'selected' => $selected, 'id'  => $id,'etatetape' => $etatetape,
            'checked' => $checked, 'selectedQ' => $selectedQ, 'selectedF' => $selectedF,]);
        }

        else {
            $error = "Vous n'avez sélectionné aucune question !";
            return $this->redirectToRoute("page_creation_etape",['error' => $error]);
        }
    }

    /**
     * Cette fonction permet de supprimer une étape
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function deleteEtape(ManagerRegistry $doctrine): Response {        
        if (isset($_POST['deleteEtape']) && $_POST['deleteEtape'] != "") {
            print($_POST['deleteEtape']);
            $etape = $doctrine->getRepository(Etape::class)->find($_POST['deleteEtape']);
            $doctrine->getManager()->remove($etape);
            $doctrine->getManager()->flush();        
            return $this->redirectToRoute('page_creation_etape');
        }
        return $this->redirectToRoute('page_acceuil_concepteur');
    }
    
    /**
     * Cette fonction permet de créer une aventure
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function creationAventure(ManagerRegistry $doctrine): Response 
    {
        $titre = "Aventure";
        $erreur = null;
        if (isset($_GET['send']) && isset($_POST['nom']) && $_POST['texte'] && $_GET['send'] != "" && $_POST['nom'] != "" ) {
            $result = $doctrine->getRepository(Aventure::class)->findAll();
            foreach ($result as $elem) {
                if ($elem->getNomaventure() == $_POST['nom']) {
                    $erreur = "Attention, cette aventure existe déjà.";
                    return $this->render('Concepteur/creationAventure.php.twig',['titre' => $titre, 'erreur' => $erreur,]);
                }
            }
            if ($_GET['send'] == 1) {
                $aventure = new Aventure();   
                $aventure->setNomAventure($_POST['nom']);
                $aventure->setEtataventure(0);
                $aventure->setTexteaventure($_POST['texte']);
                $doctrine->getManager()->persist($aventure);
                $doctrine->getManager()->flush();
                return $this->redirectToRoute('page_acceuil_concepteur');
            }
        } else if (isset($_GET['send']) && $_GET['send'] != "") {
            $erreur = "Veuillez remplir tous les champs.";
        }

        return $this->render('Concepteur/creationAventure.php.twig',['titre' => $titre, 'erreur' => $erreur,
                                                                  ]);
    }

    /**
     * Cette fonction permet l'upload et la visualisation des films associées aux étapes
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param SluggerInterface $slugger
     * @return Response
     */
    public function gestionFilm(Request $request,ManagerRegistry $doctrine, SluggerInterface $slugger): Response
    {
        $titre = "Gestion Film";
        $film = new Film();   
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);
        $erreur = null;
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $filmFile */
            $filmFile = $form->get('film')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($filmFile) {
                $originalFilename = pathinfo($filmFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$filmFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $filmFile->move(
                        $this->getParameter('film_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'filmFilename' property to store the MP4 file name
                // instead of its contents
                $film->setNomfilm($newFilename);
                $doctrine->getManager()->persist($film);
                $doctrine->getManager()->flush();
            } else {
                $erreur = "Veuillez choisir un fichier.";
            }
        }
        
        $result = $result = $doctrine->getRepository(Film::class)->findAll();     
        if (!isset($result)) {
            $result = [];
        }
        $nomFilm = [];
        foreach ($result as $elem) {
            $nomFilm[] = $elem->getNomfilm();
        }

        return $this->renderForm('Concepteur/gestionFilm.php.twig',['titre' => $titre,'films' => $nomFilm,'form' => $form, 'erreur' => $erreur]);
    }
    
    /**
     * Cette fonction permet de supprimer un film
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function deleteFilm(ManagerRegistry $doctrine): Response {
        if (isset($_GET['film'])) {

            $file = $doctrine->getRepository(Film::class)->findBy(['nomfilm' => $_GET['film']])[0];
            $doctrine->getManager()->remove($file);
            $doctrine->getManager()->flush();

            $filename = "upload/".$_GET['film'];
            unlink($filename);

            return $this->redirectToRoute('page_gestion_film');
        }
        return $this->redirectToRoute('page_acceuil_concepteur');
    }

    /**
     * Cette fonction permet de générer et de récupérer le qr-code
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function generateQRCode(ManagerRegistry $doctrine): Response {
        $id = 0;
        if ($_GET['id'] == "default") {
            return $this->redirectToRoute('page_creation_etape');
        }
        if (isset($_GET['id'])) {
            $id = $_GET['id']; 
        }

        $titre = "Récupérer le QRCode de votre étape";

        return $this->render('Concepteur/generateQRCode.php.twig',["id" => $id, 'titre' => $titre]);
    }
}