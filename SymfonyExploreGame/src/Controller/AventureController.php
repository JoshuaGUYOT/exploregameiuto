<?php

namespace App\Controller;

use App\Repository\AventureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Aventure;
use Doctrine\Persistence\ManagerRegistry;

/**
 * classe AventureController gère la création et l'affichage d'une aventure
 */
class AventureController extends AbstractController
{
    /**
     * Créer une aventure et l'insert dans la BD
     * @param ManagerRegistry $doctrine
     * @param string $name
     * @param string $texte
     * @return Response
     */
    public function createAventure(ManagerRegistry $doctrine, string $name, string $texte): Response
    {
        $entityManager = $doctrine->getManager();

        $aventure = new Aventure();
        $aventure->setNomaventure($name);
        $aventure->setTexteaventure($texte);
        $aventure->setEtataventure(0);


        // tell Doctrine you want to (eventually) save the aventure (no queries yet)
        $entityManager->persist($aventure);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Nouvelle aventure enregistrer avec le nom: '.$aventure->getNomaventure());
    }
    /**
     * @Route("/aventure/{id}", name="aventure_show")
     */
    public function show(ManagerRegistry $doctrine, string $id): Response
    {
        $repository = $doctrine->getRepository(Aventure::class);
        $aventuare = $repository->find($id);

        if (!$aventuare) {
            throw $this->createNotFoundException(
                'No aventuare found for id '.$id
            );
        }

        return new Response('Check out this great aventuare: '.$aventuare->getNomaventure());

        // or render a template
        // in the template, print things with {{ aventuare.name }}
        // return $this->render('aventuare/show.html.twig', ['aventuare' => $aventuare]);
    }

}
