<?php


namespace App\Controller;


use App\Repository\AventureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Classe StatistiquesController gère les statistiques
 */
class StatistiquesController extends AbstractController
{
    /**
     * Fonction getAventures reprend toutes les aventures pour les afficher sur la page
     *
     * @param AventureRepository $ave
     * @return Response
     */
    public function getAventures(AventureRepository $ave): Response {

        return $this->render('Admin/aventureStats.php.twig', [
            'titre' => 'Statistiques',
            'aventures' => $ave->Aventuredisplay()
        ]);
    }

    /**
     * Fonction statistiques affiche toutes les statistiques
     *
     * @param AventureRepository $ave
     * @param Request $request
     * @return Response
     */
    public function statistiques(AventureRepository $ave, Request $request): Response {
        $nomAventure = $request->get('aventure');
        $nbEqu = $ave->getNbParticipants($nomAventure);
        $nbEtapes = $ave->getNbEtapes($nomAventure);
        $moyAv = $ave->moyenneAventure($nomAventure);
        return $this->render('Admin/statistiques.php.twig', [
            'titre'=> 'Statistiques',
            'nbequipes' => $nbEqu,
            'nbetapes' => $nbEtapes,
            'moyenne' => $moyAv,
            'scoreMax' => $nbEqu
        ]);
    }
}