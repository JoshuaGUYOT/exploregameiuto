<?php

namespace App\Controller;

use App\Entity\Aventure;
use App\Entity\Equipe;
use App\Entity\Faire;
use App\Entity\Utilisateur;
use App\Entity\Associer;
use App\Repository\EtapesRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\AdminEquipeForm;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Classe AdminController gère différentes pages de l'administrateur
 */

class AdminController extends AbstractController
{
    /**
     * Fonction creaEquipe qui insert une équipe à une aventure dans la BD et qui renvoit sur la même page
     *
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param UtilisateurRepository $uti
     * @return Response
     */
    public function creaEquipe(ManagerRegistry $doctrine, Request $request, UtilisateurRepository $uti): Response
    {
        $u = $uti->getJoueurs();
        $u2 = $doctrine->getRepository(Aventure::class)->findBy(["etataventure" => 1]);
        $username = array();

        foreach($u as $elem){
            $username += [$elem->getUsername() => $elem->getId()];
        }

        $nameAventure = array();
        foreach($u2 as $elem){
            $nameAventure += ["".$elem->getNomaventure() => $elem->getNomaventure()];
        }
        $form = $this->createForm(AdminEquipeForm::class, [[$username, $nameAventure]]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            if(sizeof($doctrine->getRepository(Equipe::class)->findBy(["nomequipe" => $form->get("nomEquipe")->getData(), "idJoueur" => $form->get("personne")->getData()])) > 0){
                return $this->renderForm('Admin/adminEquipe.php.twig', [
                    'titre' => "Création d'une équipe",
                    'form' => $form,
                    'cree' => "false",
                    'erreur' => "true",
                ]);
            }
            try{
                // insertion equipe
                $equipe = new Equipe();
                $equipe->setNomequipe($form->get("nomEquipe")->getData());
                $equipe->setIdJoueur(intval($form->get("personne")->getData()));
                $equipe->setNomjoueurs($form->get("membres")->getData());
                $doctrine->getManager()->persist($equipe);
                $doctrine->getManager()->flush();

                // insertion faire
                $eq = $doctrine->getRepository(Equipe::class)->findOneBy(
                    ["nomequipe" => $form->get("nomEquipe")->getData(),
                    "idJoueur" => $form->get("personne")->getData()]
                );

                $faire = new Faire();
                $faire->setIdEquipe($eq->getIdequipe());
                $faire->setNomAventure($form->get("aventure")->getData());
                $faire->setScore(0);
                $doctrine->getManager()->persist($faire);
                $doctrine->getManager()->flush();

                // insertion associer
                $assoListe = $doctrine->getRepository(Associer::class)->findBy(
                    ["aventureIsActuelle" => 1]
                );

                foreach($assoListe as $a){
                    $e = $doctrine->getRepository(Equipe::class)->findOneBy(
                        ["idequipe" => $a->getIdequipe()]
                    );
                    if ($e->getIdJoueur() == $eq->getIdJoueur()){
                        $a->setAventureIsActuelle(0);
                        $doctrine->getManager()->flush();
                    }
                }

                $asso = new Associer();
                $asso->setIdEquipe($eq->getIdequipe());
                $asso->setNomAventure($form->get("aventure")->getData());
                $asso->setIdAdmin($this->getUser()->getId());
                $asso->setAventureIsActuelle(1);
                $doctrine->getManager()->persist($asso);
                $doctrine->getManager()->flush();

                $form = $this->createForm(AdminEquipeForm::class, [[$username, $nameAventure]]);
                return $this->renderForm('Admin/adminEquipe.php.twig', [
                    'titre' => "Création d'une équipe",
                    'form' => $form,
                    'cree' => "true",
                    'erreur' => "false",
                ]);
            } catch (UniqueConstraintViolationException $e){
                print_r("ah");
            }
            
        }
        return $this->renderForm('Admin/adminEquipe.php.twig', [
            'titre' => "Création d'une équipe",
            'form' => $form,
            'cree' => "false",
            'erreur' => "false",
        ]);
    }

    /**
     * Fonction classement qui retourne le classement d'une aventure
     *
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return Response
     */
    public function classement(ManagerRegistry $doctrine, Request $request): Response
    {
        $aventures = $doctrine->getRepository(Aventure::class)->findAll();
        $nomAventures = [];
        foreach ($aventures as $elem) {
            $nomAventures[] = $elem->getNomAventure();
        }

        if($request->isMethod("post")) {

            $entities = $doctrine->getRepository(Faire::class)->findBy([
                "nomaventure" => $request->get('aventure'),
            ]);

            $tabl = [];

            foreach($entities as $elem){
                $equipe = $doctrine->getRepository(Equipe::class)->findOneBy([
                    "idequipe" => $elem->getIdequipe(),
                ]);
                $tabl[$equipe->getNomequipe()] = $elem->getScore();
                arsort($tabl);
            }

            return $this->render('Admin/tableau.html.twig', [
                'tableau' => $tabl
            ]);
        }
        return $this->render('Admin/classements.php.twig', [
            'titre' => 'Classement des équipes',
            'aventures' => $nomAventures
        ]);
    }
}