<?php


namespace App\Controller;

use App\Entity\Aventure;
use App\Entity\Etape;
use App\Repository\AventureRepository;
use App\Repository\EtapesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Classe ReponsesController qui gère l'affichage des réponses pour une aventure et une étape
 */
class ReponsesController extends AbstractController
{
    /**
     * Fonction selectAventure qui permet de séléctioner une aventure pour ensuite afficher les étapes
     *
     * @param AventureRepository $ave
     * @return Response
     */
    public function selectAventure(AventureRepository $ave): Response {
        $rep = $ave->Aventuredisplay();
        return $this->render('Admin/reponsesAventure.php.twig', [
            'titre' => 'Réponses par étapes',
            'aventures' => $rep
        ]);
    }

    /**
     * Fonction etapes qui affiche un select avec les étapes de l'aventure
     *
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param AventureRepository $ave
     * @return Response
     */
    public function etapes(ManagerRegistry $doctrine, Request $request, AventureRepository $ave): Response {
        $rep = $ave->getEtapes($request->get('aventure'));
        if($request->isMethod("post")) {
            return $this->render('Admin/etapes.php.twig', [
                'titre' => 'Choix des étapes par aventure',
                'etapes' => $rep,
            ]);
        }
        return $this->selectAventure($ave);
    }

    /**
     * Fonction reponses qui affiche les réponses d'une aventure et d'une étape
     *
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param EtapesRepository $etape
     * @return Response
     */
    public function reponses(ManagerRegistry $doctrine, Request $request, EtapesRepository $etape): Response {
        $rep = $etape->getReponsesEtape((integer)$request->get('etape'));
        if ($request->isMethod("post")) {
            return $this->render('Admin/reponsesParEtapes.php.twig', [
                'titre' => 'Liste des réponses obtenues à une étape',
                'rep' => $rep,
            ]);
        }
        return $this->redirect('/admin/aventure');
    }

}