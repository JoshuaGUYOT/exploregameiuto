<?php

namespace App\Controller;


use App\Form\AdminChangementsForm;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * classe ChangementsRolesController gère la page des changements des rôles des utilisateurs par l'administrateur
 */
class ChangementsRolesController extends AbstractController
{
    /**
     * Permet de changer de rôle un utilisateur
     * @param UtilisateurRepository $uti
     * @param Request $request
     * @return Response
     */
    public function changementsRoles(UtilisateurRepository $uti, Request $request): Response {

        $users = $uti->getUtilisateurs();
        $newUsers = array();
        foreach ($users as $u) {
            foreach ($u as $u2) {
                if ($u2 != $this->getUser()->getUsername())
                $newUsers += [$u2 => $u2];
            }
        }
        $roles = ["Administrateur", "Concepteur", "Joueur"];
        $newRoles = array();
        foreach ($roles as $r) {
            $newRoles += [$r => $r];
        }
        $form = $this->createForm(AdminChangementsForm::class, [$newUsers, $newRoles]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $nameUser = $form->get("utilisateur")->getData();
            $roleUser = $form->get("role")->getData();
            switch ($roleUser) {
                case "Joueur":
                    $roleUser = "ROLE_USER";
                    break;
                case "Concepteur":
                    $roleUser = "ROLE_CONCEPTEUR";
                    break;
                case "Administrateur":
                    $roleUser = "ROLE_ADMIN";
                    break;
            }
            $uti->updtateRole($nameUser, $roleUser);
            return $this->renderForm('Admin/changementsRoles.php.twig', [
                'titre' => 'Changement des rôles',
                'form' => $form,
                'update' => true
            ]);
        }
        return $this->renderForm('Admin/changementsRoles.php.twig', [
            'titre' => 'Changement des rôles',
            'form' => $form,
            'update' => false
        ]);
    }
}
?>