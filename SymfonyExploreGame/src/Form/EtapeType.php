<?php

namespace App\Form;

use App\Entity\Etape;
use App\Entity\Aventure;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire EtapeType pour la création d'une étape
 */
class EtapeType extends AbstractType
{
    /**
     * Construit le formulaire pour la création d'une étape
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nometape')
            ->add('posxqrcode')
            ->add('posyqrcode')
            ->add('etatetape')
            ->add('nomaventure',EntityType::class, array('class' => 'App:Aventure','choice_label' => 'nomaventure'))

        ;
    }

    /**
     * Configure les options du form si besoin
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Etape::class,
        ]);
    }
}
