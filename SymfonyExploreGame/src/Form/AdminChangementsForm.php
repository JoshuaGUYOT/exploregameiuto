<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Formulaire AdminChangementsForm pour les changements des rôles des utilisateurs
 */
class AdminChangementsForm extends AbstractType
{
    /**
     * Construit le formulaire pour les changements de rôles
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('utilisateur', ChoiceType::class, [
                'choices' => $options["data"][0],
            ])
            ->add('role', ChoiceType::class, [
                'choices' => $options["data"][1],
            ])
            ->add('valider', SubmitType::class)
        ;
    }

    /**
     * Configure les options du form si besoin
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
?>