<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Formulaire AdminEquipeForm pour créer une équipe et l'associer à une aventure avec un référent
 */
class AdminEquipeForm extends AbstractType
{
    /**
     * Construit le formulaire pour la création d'une équipe
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('nomEquipe', TextType::class)
            ->add('personne', ChoiceType::class, [
                'choices' => $options["data"][0][0],
            ])
            ->add('membres', TextType::class)
            ->add('aventure', ChoiceType::class, [
                'choices' => $options["data"][0][1],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'sauvegarder'
            ])
        ;
    }

    /**
     * Configure les options du form si besoin
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
